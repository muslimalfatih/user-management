// api.ts
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

const baseURL = 'https://tasks.vitasoftsolutions.com';

export const api = axios.create({
  baseURL,
  // headers: {
  //   'Content-Type': 'application/json',
  // },
});

export const getRequest = async <T>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> => {
  try {
    const response = await api.get<T>(url, config);
    return response;
  } catch (error) {
    throw error;
  }
};

export const postRequest = async <T>(url: string, data: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> => {
  try {
    const response = await api.post<T>(url, data, config);
    return response;
  } catch (error) {
    throw error;
  }
};

export const putRequest = async <T>(url: string, data: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> => {
  try {
    const response = await api.put<T>(url, data, config);
    return response;
  } catch (error) {
    throw error;
  }
};
