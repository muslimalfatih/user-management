import Link from 'next/link';
import Image from 'next/image';
import { getInitials } from '@/lib/getInitials';
import { User } from '@/types/form';

interface UserListItemProps {
  user: User;
}

const UserListItem: React.FC<UserListItemProps> = ({ user }) => {
  return (
    <div className="bg-white shadow-md p-6 rounded-md mb-4">
      <div className="flex items-center justify-between">
        <div className="flex items-center">
          {user.profile_picture ? (
            <Image src={user.profile_picture} alt={user.name} width={40} height={40} className="w-16 h-16 rounded-full mr-4 object-cover" />
          ) : (
            <div className="w-16 h-16 bg-gray-300 rounded-full flex items-center justify-center mr-4">
              <p className="text-gray-600 text-xl font-semibold">
                {getInitials(user.name)}
              </p>
            </div>
          )}
          <div>
            <p className="text-xl font-semibold">{user.name}</p>
            <p className="text-gray-600">Birthdate: {user.birthdate}</p>
            <p className="text-gray-600">Joining Date: {user.joining_date}</p>
            <p className={`text-sm ${user.active_status ? 'text-green-500' : 'text-red-500'}`}>
              {user.active_status ? 'Active' : 'Inactive'}
            </p>
          </div>
        </div>
        <div>
          <Link href={`/edit/${user.id}`} className="text-blue-500 hover:underline">Edit</Link>
        </div>
      </div>
    </div>
  );
};

export default UserListItem;
