"use client";

import Image from 'next/image';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useEffect, useState } from 'react';
import { useParams, useRouter } from 'next/navigation'
import ReactQuill from 'react-quill';
import { useDropzone } from 'react-dropzone';
import { User, UserDetail } from '../types/form'
import { Toaster, toast } from 'sonner'
import { getRequest, putRequest, postRequest } from '@/lib/api';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-quill/dist/quill.snow.css';
import dynamic from 'next/dynamic';

const QuillEditor = dynamic(() => import("react-quill"), {
  ssr: false,
  loading: () => <p>Loading Editor</p>,
});

interface FormData {
  name: string;
  phone_number: string;
  description: string;
  birthdate: string;
  active_status: boolean;
  profile_picture: string;
}

interface Props {
  type: string;
}

const UserForm: React.FC<Props> = ({ type }) => {
  const {
    control,
    handleSubmit,
    register,
    setValue,
    getValues,
    formState: { errors },
  } = useForm<FormData>();
  const formValues = getValues();
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [editorValue, setEditorValue] = useState<string>('');

  const onDrop = (acceptedFiles: File[]) => {
    const file = acceptedFiles[0];
    setSelectedFile(file);
  };


  const { getRootProps, getInputProps } = useDropzone({ onDrop });
  const router = useRouter()
  const params = useParams()
  const { id } = params

  useEffect(() => {
    const fetchUserDetails = async () => {
      if (type === 'edit' && id) {
        try {
          const response = await getRequest<UserDetail>(`https://tasks.vitasoftsolutions.com/userdata/${id}/`);
          const userData = response.data;

          setValue('name', userData.name);
          setValue('phone_number', userData.phone_number);
          setValue('description', userData.description);
          setValue('birthdate', userData.birthdate);
          setValue('active_status', userData.active_status);
          setValue('profile_picture', userData.profile_picture);
        } catch (error) {
          console.error('Error fetching user details:', error);
        }
      }
    };

    fetchUserDetails();
  }, [type, id, setValue]);

  const onEditorChange = (value: string) => {
    setEditorValue(value);
    setValue('description', value);
  };

  const onSubmit: SubmitHandler<FormData> = async (data) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    };

    try {
      const formData = new FormData();

      formData.append('name', data.name);
      formData.append('phone_number', data.phone_number);
      formData.append('description', data.description);
      formData.append('birthdate', data.birthdate);
      formData.append('active_status', data.active_status.toString());

      
      if (selectedFile) {
        formData.append('profile_picture', selectedFile);
      }

      if (type === 'edit' && id) {
        const response = await putRequest(`https://tasks.vitasoftsolutions.com/userdata/${id}/`, formData, config);
        
        toast.success('User updated successfully')
        router.push('/')
      } else {
        const response = await postRequest('https://tasks.vitasoftsolutions.com/userdata/', formData, config);
        toast.success('User created successfully')
        router.push('/')
      }
    } catch (error) {
      console.error(`Error submitting the form: ${error}`);
      toast.error(`Error submitting the form: ${error}`)
    }
  };
  

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="w-full">
        <div className="mb-4">
          <label htmlFor="name" className="block text-sm font-medium text-gray-600">
            Name
          </label>
          <input
            type="text"
            {...register('name', { required: 'Name is required' })}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
          />
          {errors.name && <p className="text-red-500 text-sm">{errors.name.message}</p>}
        </div>

        <div className="mb-4">
          <label htmlFor="phone_number" className="block text-sm font-medium text-gray-600">
            Phone Number
          </label>
          <input
            type="text"
            {...register('phone_number')}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
          />
        </div>

        <div className="mb-4">
          <label htmlFor="description" className="block text-sm font-medium text-gray-600">
            Description
          </label>
          <QuillEditor
            value={formValues.description}
            onChange={onEditorChange}
            modules={{ toolbar: true }}
            className="h-[150px] mb-16"
          />
        </div>

        <div className="mb-4">
          <label htmlFor="birthdate" className="block text-sm font-medium text-gray-600">
            Birthdate
          </label>
          <input
            type="date"
            {...register('birthdate', { required: 'Birthdate is required' })}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
          />
          {errors.birthdate && <p className="text-red-500 text-sm">{errors.birthdate.message}</p>}
        </div>

        <div className="mb-4">
          <label htmlFor="active_status" className="block text-sm font-medium text-gray-600">
            Active Status
          </label>
          <input
            type="checkbox"
            {...register('active_status')}
            className="mt-1 p-2 border border-gray-300 rounded"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="profile_picture" className="block text-sm font-medium text-gray-600">
            Profile Picture
          </label>
          <div {...getRootProps()} className="mt-1 p-2 border border-gray-300 rounded w-full">
            <input {...getInputProps()} />
            <>
              <p>Drag &apos;n&apos; drop an image here, or click to select an image</p>
              {selectedFile && (
                <Image
                  src={formValues.profile_picture ? formValues.profile_picture : URL.createObjectURL(selectedFile)}
                  alt="Profile Preview"
                  className="mt-2 rounded-md"
                  width={200}
                  height={200}
                  style={{ maxWidth: '100%', maxHeight: '200px' }}
                />
              )}
              </>
          </div>
        </div>

        <button type="submit" className="bg-blue-500 text-white p-2 rounded">
          Submit
        </button>
      </form>
      <Toaster position="top-right" />
    </>
    
  );
};

export default UserForm;
