# User Management 

This project is a user management system that allows users to create, edit, and view user details.

## Features

- Create new users with details like name, phone number, description, birthdate, and profile picture.
- Edit existing user details.
- View a list of users with basic information.
- Upload and display user profile pictures.

## Technologies Used

- React
- Next.js
- Typescript
- Tailwind CSS
- React Hook Form
- React Quill

## Getting Started

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/muslimalfatih/user-management.git
   cd user-management
   npm install
2. Open http://localhost:3000 in your browser.

## Usage
- Access the application in your browser.
- Create a new user by providing the required details.
- Edit existing user details by clicking on the edit button.
- View the list of users with their basic information.

## API
- API Documentation: https://tasks.vitasoftsolutions.com/redoc/
- API URL: https://tasks.vitasoftsolutions.com/



