'use client';

import Link from 'next/link';
import { useState, useEffect } from 'react';
import UserListItem from '@/components/UserList';
import { getRequest } from '@/lib/api';
import { User } from '@/types/form';

type UserState = User[];

const UserListPage = () => {
  const [users, setUsers] = useState<UserState>([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await getRequest<User[]>('/userdata');
        const data = response.data;
        setUsers(data);
				setLoading(false);
      } catch (error) {
        console.error('Error fetching user data:', error);
				setLoading(false);
      }
    };

    fetchUserData();
  }, [page]);

  const handleLoadMore = () => {
    setPage(prevPage => prevPage + 1);
  };


  return (
    <div className="flex flex-col py-4">
      <div className="flex items-center justify-between mb-4">
        <h1 className="font-semibold text-2xl">User List</h1>
        <Link href="/create" className="bg-green-500 text-white px-4 py-2 rounded">Create User</Link>
      </div>
      {loading && (
        <div className="text-center">
          Loading...
        </div>
      )}

      {!loading &&
        users.slice(0, 10 * page).map((user, idx) => (
          <UserListItem key={idx} user={user} />
        ))
      }
      {!loading && users.length > 10 * page && (
        <button onClick={handleLoadMore} className="bg-blue-500 text-white p-2 rounded mt-4">
          Load More
        </button>
      )}
    </div>
  );
};

export default UserListPage;
