import UserForm from "@/components/UserForm"

export default function EditPage() { 
  return (
    <div className="flex flex-col py-4">
      <h1 className="font-semibold text-2xl mb-2">Edit Page</h1>
      <UserForm type="edit"/>
    </div>
  )
 };