import UserForm from "@/components/UserForm";

export default function CreatePage() {
  return (
    <div className="flex flex-col py-4">
      <h1 className="font-semibold text-2xl mb-2">Create Page</h1>
      <UserForm type="create"/>
    </div>
  );
}