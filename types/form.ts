export interface FormData {
  name: string;
  phone_number: string;
  description: string;
  birthdate: string;
  active_status: boolean;
  profile_picture: string;
}

export interface User {
  id: number;
  name: string;
  profile_picture: string;
  birthdate: string;
  joining_date: string;
  active_status: boolean;
}

export interface UserDetail {
  id: number;
  name: string;
  profile_picture: string;
  phone_number: string;
  description: string;
  birthdate: string;
  joining_date: string;
  active_status: boolean;
}